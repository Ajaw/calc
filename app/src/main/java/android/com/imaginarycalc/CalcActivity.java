package android.com.imaginarycalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.LinkedList;
import java.util.List;

public class CalcActivity extends AppCompatActivity {



    private Spinner numberType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);
        numberType = (Spinner) findViewById(R.id.numberType);
        this.setupNumberType();
        this.numberType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupNumberType(){
        List<String> numberType = new LinkedList<>();
        numberType.add("Trygonaemetryczny");
        numberType.add("Wykładniczy");
        numberType.add("Algebraiczna");

        ArrayAdapter<String> spinAda = new ArrayAdapter<String>(getBaseContext(),R.layout.support_simple_spinner_dropdown_item,numberType);
        this.numberType.setAdapter(spinAda);
    }
}
